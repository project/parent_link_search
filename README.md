# Drupal 8 - Parent Link Search
A Simple lightweight module that gives you the ability to search & highlight the Parent Link select drop down on forms.

##Why is this needed?
Once the menu becomes large it becomes very difficult to find what you are looking for, there is no built (or browser) way of searching items in a select list dropdown.
There are some alternatives (e.g. [select2](https://www.drupal.org/project/select2)) however search in select2 requires the third party library and search results do not maintain the hierarchy of menu items, so you may not be able to view parent items of the item you searched for.

This modules will simply highlight the menu items and change the parent link select list to the first found item.

Note: Highlight color is currently not adjustable


## Installation
Install this module like every other Drupal module.

## Usage
New form elements will appear under the 'Parent Link' select list dropdown
