
(function ($, Drupal) {

  /*
   * Finds $search text in html select list $list with options, highlights optiont ags, and
   * returns count of items found
   */
  function searchTool($search, $list){
    var $count = 0;
    $list.find('option').each(function(){
      if (this.innerHTML.toLowerCase().includes($search)) {
        if ($count == 0) {
          //set the select list to first item found
          $list.val(this.value);
        }
        $count++;
        $(this).addClass('parent_link_search_highlight')
      }else{
        $(this).removeClass('parent_link_search_highlight')
      }
    })
    return $count;
  }


  Drupal.behaviors.parentLinkSearch = {
    attach: function attach(context) {
      var $context = $(context);
      $context.find('.menu-link-form').each(function () {
        var $selectList = $(this).find('#edit-menu-menu-parent')
        var $search = $(this).find('#search_parent_link');
        var $found_text = $('#parent_link_found');
        var $submit = $('#edit-menu-menu-parent--description .form-submit');

        if (!($search.length)) {
          return;
        }
        $submit.on('click', function() {
          if ($search.val().length > 2) {
            $count = searchTool($search.val().toLowerCase(), $selectList);
            $found_text.html("Found items: " + $count);
          }else{
            if($search.val() == ""){
              $found_text.html("");
              $selectList.find('.parent_link_search_highlight').removeClass("parent_link_search_highlight");
            }
          }
        })

      });
    }
  };
})(jQuery, Drupal);
